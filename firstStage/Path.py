from Point import Point

class Path:
    """Represents a route of a drone"""

    def __init__(self, points):
        
        if len(points) <= 1:
            raise Exception("A path cannot be empty, and cannot have only one point")
        
        if points[0].x != points[-1].x or points[0].y != points[-1].y:
            raise Exception("A Drone's path must begin and end in the same place")
       
        self.points = points

    def print_path(self):
        """printing all the points in the path"""
        i = 1
        for point in self.points:
            print(f"{i}: ({point.x}, {point.y}, time={point.time})")
            i += 1

