import math

class Point:
    """Represents a point (x, y) in the plane"""

    def __init__(self, x=0, y=0, time=0):
        self.x = x
        self.y = y
        self.time = time

    def distance(self, other):
        return math.sqrt((other.x - self.x) ** 2 + (other.y - self.y) ** 2)
