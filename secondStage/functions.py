from Path import Path
from Point import Point


def can_start_simultaneously(path1, path2):
    for path1_point, path2_point in zip(path1.points, path2.points):
        if path1_point.time == path2_point.time and path1_point.distance(path2_point) < 5:
            return False
    return True


path1 = Path([Point(0, 0, time=0), Point(100, 50, time=60), Point(0, 0, time=120)])
path2 = Path([Point(0, 10, time=0), Point(100, 55, time=40), Point(0, 10, time=100)])
print(can_start_simultaneously(path1, path2))