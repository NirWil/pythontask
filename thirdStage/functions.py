import math

from Path import Path
from Point import Point

SPLIT_PER_SECONDS = 1

def get_speed_two_points(point1, point2):
    """This function calculates the speed (according to time) of the two points, it returns the general speed,
    the x speed, the y speed and it returns it as a dict"""
    points_time = point2.time - point1.time

    if points_time == 0: #devding by 0 is forbbiden
        return {"speed": 0, "x_speed": 0, "y_speed": 0}

    points_distance = point1.distance(point2)
    speed = points_distance / points_time

    x_distance = point2.x - point1.x
    x_speed = x_distance / points_time

    y_distance = point2.y - point1.y 
    y_speed = y_distance / points_time

    return {"speed": speed, "x_speed": x_speed, "y_speed": y_speed}

def change_points_artifically(path):
    """This function creates a new path that is split to points to more points while keeping the location and time and parameters"""
    new_path = []

    #I need to count index to keep track of the next point is
    i = 1
    previous_point = path.points[0]
    new_path.append(path.points[0])

    for point in path.points:
        
        #if there is a next point, be the next point
        if i <= len(path.points) - 1:
            next_point = path.points[i]

        #how many points are needed to be add
        points_by_time = (next_point.time - point.time) / SPLIT_PER_SECONDS 

        #at what speed do they move
        points_speed = get_speed_two_points(point, next_point) 

        for _ in range(int(points_by_time)):
            new_x = previous_point.x + (points_speed["x_speed"] * SPLIT_PER_SECONDS)
            new_y = previous_point.y + (points_speed["y_speed"] * SPLIT_PER_SECONDS)
            new_time = previous_point.time + SPLIT_PER_SECONDS

            new_path.append(Point(new_x, new_y, new_time))
            previous_point = new_path[-1]

        i += 1
    #I explained why I use round here on the last
    new_path[-1].x = round(new_path[-1].x)
    new_path[-1].y = round(new_path[-1].y)
    return Path(new_path)


def can_start_simultaneously(path1, path2):
    """This function checks if there is an inteception between the two points. its complexity is O(n^2)"""
    for path1_point in path1.points:
        for path2_point in path2.points:
            if path1_point.time == path2_point.time and path1_point.distance(path2_point) < 5:
                return False
    return True

#feel free to play with the values.
#because we split it every x seconds, it can predict interception between the two paths.
path1 = Path([Point(20, 200, time=50), Point(40, 60, time=91), Point(40, 65, time=105), Point(20, 200, time=140)])
path2 = Path([Point(0, 0, time=0), Point(10, 50, time=26), Point(20, 100, time=42), Point(20, 120, time=90), Point(0, 0, time=121)])
path1 = change_points_artifically(path1)
path2 = change_points_artifically(path2)
print(can_start_simultaneously(path1, path2))


